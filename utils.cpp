//
// Created by zaporozhec on 11/27/18.
//

#include <iostream>
#include "utils.h"
#include "cmath"

int utils::getRandom(int limit) {
    return random() % limit;
}


void utils::printArray(std::vector<Point *> *array) {
    for (auto &it: *array) {
        std::cout << it->number << " " << it->x << " " << it->y << "\n";
    }
}


void utils::printArray(std::vector<IntegerSolution *> *array) {
    for (auto &it: *array) {
        std::cout << "size:" << it->solution.size() << "; " << it->fitness << "; ";
        std::cout << "\n";
    }
}

void utils::deleteArray(std::vector<IntegerSolution *> *array) {
    array->erase(array->begin(), array->end());
    delete array;
}

void utils::deleteArray(std::vector<Point *> *array) {
    array->erase(array->begin(), array->end());
    delete array;
}
