//
// Created by zaporozhec on 11/28/18.
//

#ifndef TEST_DISTANCE_UTILS_H
#define TEST_DISTANCE_UTILS_H

#include "Point.h"
#include <vector>

class distance_utils {
public:
    static double getDistance(int num1, int num2, std::vector<Point *> *towns);

    static double getPath(std::vector<int> solution, std::vector<Point *> *towns);
};


#endif //TEST_DISTANCE_UTILS_H
