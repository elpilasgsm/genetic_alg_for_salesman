#include <iostream>
#include <iterator>
#include <algorithm>
#include <random>
#include <fstream>
#include <chrono>
#include <sstream>
#include <thread>
#include <pthread.h>
#include <mutex>

#include "distance_utils.h"
#include "utils.h"

using namespace std::chrono;


std::vector<IntegerSolution *> *
getInitialPopulation(int populationSize, unsigned long numberOfElements, std::vector<Point *> *towns);

std::vector<Point *> *getTowns(const std::string &filename);

std::vector<IntegerSolution *> *crossover(IntegerSolution *parent1, IntegerSolution *parent2);

IntegerSolution *inversion(IntegerSolution *parent1);

IntegerSolution *pairMutation(IntegerSolution *parent, std::vector<Point *> *towns);

bool solutionComparator(const IntegerSolution *sol1, const IntegerSolution *sol2);

double averageFitnessForPopulation(std::vector<IntegerSolution *> *population);

std::mutex lock;

void *crossoverThread(
        IntegerSolution *parent1, IntegerSolution *parent2,
        std::vector<Point *> *towns,
        std::vector<IntegerSolution *> *population) {


    std::vector<IntegerSolution *> *children = crossover(parent1, parent2);
    if (children != nullptr) {
        for (auto &child:*children) {
            child->fitness = distance_utils::getPath(child->solution, towns);
            lock.lock();
            population->push_back(child);
            lock.unlock();
        }
    }
}

int main(int numC, char **argsV) {
    const int populationSize = std::stoi(argsV[2]);
    unsigned long numberOfElements;
    std::ostringstream resultFileBodyStream;

    const double crossoverProbability = std::stod(argsV[3]);
    const double mutationProbability = std::stod(argsV[4]);
    const int iterations = std::stoi(argsV[5]);
    const int iterationsDiff = std::stoi(argsV[6]);


    int iterationWithBest = 0;
    IntegerSolution *best_solution = nullptr;
    // generate points (towns)
    auto *towns = getTowns(argsV[1]);
    numberOfElements = towns->size();
    resultFileBodyStream << "{";

    resultFileBodyStream << "\"populations_size\": " << populationSize << ", ";
    resultFileBodyStream << "\"number_of_points\": " << numberOfElements << ", ";
    resultFileBodyStream << "\"crossover_probability\": " << crossoverProbability << ", ";
    resultFileBodyStream << "\"iterations\": " << iterations << ", ";
    resultFileBodyStream << "\"stagnation_iterations_number\": " << iterationsDiff << ", ";
    resultFileBodyStream << "\"logs\":[";
    //generate population with given size
    auto population =
            getInitialPopulation(populationSize, numberOfElements, towns);
    const auto crossoverNumber = static_cast<unsigned int>(crossoverProbability * populationSize);
    const auto mutationNumber = static_cast<unsigned int>(mutationProbability * populationSize);
    //do iterations
    milliseconds begin = duration_cast<milliseconds>(system_clock::now().time_since_epoch());

    for (int iteration = 0; iteration < iterations; iteration++) {
        //perform crossovers
        std::thread crossovers[crossoverNumber];
        for (int i = 0; i < crossoverNumber; i++) {
            IntegerSolution *parent1 = population->at(static_cast<unsigned long>(random() % populationSize));
            IntegerSolution *parent2 = population->at(static_cast<unsigned long>(random() % populationSize));
            while (parent1 == parent2) {
                parent2 = population->at(static_cast<unsigned long>(random() % populationSize));
            }
            std::vector<IntegerSolution *> *children = crossover(parent1, parent2);
            if (children != nullptr) {
                for (auto &child:*children) {
                    child->fitness = distance_utils::getPath(child->solution, towns);
                    population->push_back(child);
                }
            }
        }

        for (int i = 0; i < mutationNumber; i++) {
            IntegerSolution *children = inversion(
                    population->at(static_cast<unsigned long>(random() % populationSize)));
            if (children != nullptr) {
                children->fitness = distance_utils::getPath(children->solution, towns);
                population->push_back(children);
            }

/*            IntegerSolution *children2 = pairMutation(population->at(static_cast<unsigned long>(random() % populationSize)), towns);
            if (children2 != nullptr) {
                children2->fitness = distance_utils::getPath(children2->solution, towns);
                population->push_back(children);
            }*/
        }


        std::sort(population->begin(), population->end(), solutionComparator);
        if (best_solution == nullptr || best_solution->fitness > population->at(0)->fitness) {
            if (best_solution != nullptr) {
                resultFileBodyStream << ", ";
            }
            best_solution = population->at(0);
            iterationWithBest = iteration;
            resultFileBodyStream
                    << "{\"time_since_start\": "
                    << (duration_cast<milliseconds>(system_clock::now().time_since_epoch()) - begin).count()
                    << ", \"iteration_number\": "
                    << iteration
                    << ", \"current_best_solution\":"
                    << best_solution->fitness
                    << ", \"current_average_fitness\": "
                    << averageFitnessForPopulation(population)
                    << "}";
        }
        if (abs(iteration - iterationWithBest) > iterationsDiff) {
            break;
        }
        population = new std::vector<IntegerSolution *>(population->begin(), population->begin() + populationSize);
    }
    resultFileBodyStream << "],";
    resultFileBodyStream << "\"result\": [";
    const char *separator = "";

    if (best_solution != nullptr)
        for (int i:best_solution->solution) {
            resultFileBodyStream << separator << i;
            separator = ",";
        }

    resultFileBodyStream << "]";
    resultFileBodyStream << "}";
    std::ostringstream resourfname;
    resourfname << towns->size() << "_points_" << begin.count() << "_starttime_result.txt";
    std::ofstream outfile(resourfname.str());
    outfile << resultFileBodyStream.str();
    outfile.close();
    std::cout << "Runtime:" << (duration_cast<milliseconds>(system_clock::now().time_since_epoch()) - begin).count();

    utils::deleteArray(population);
    utils::deleteArray(towns);

    return 0;
}

bool solutionComparator(const IntegerSolution *sol1, const IntegerSolution *sol2) {
    return sol1->fitness < sol2->fitness;
}

std::vector<IntegerSolution *> *
getInitialPopulation(int populationSize, unsigned long numberOfElements, std::vector<Point *> *towns) {

    std::random_device rd;
    std::mt19937 g(rd());

    auto *population = new std::vector<IntegerSolution *>();
    for (int i = 0; i < populationSize; i++) {
        auto *curSol = new IntegerSolution[numberOfElements];
        std::vector<Point *> currTowns;
        copy(towns->begin(), towns->end(), back_inserter(currTowns));
        std::shuffle(currTowns.begin(), currTowns.end(), g);
        for (auto &town : currTowns) {
            curSol->solution.push_back(town->number);
        }
        curSol->fitness = distance_utils::getPath(curSol->solution, towns);
        population->push_back(curSol);
    }
    return population;
}

std::vector<Point *> *getTowns(const std::string &filename) {
    auto *towns = new std::vector<Point *>();
    std::ifstream infile(filename);
    int x, y;
    char separator;
    int counter = 1;
    while ((infile >> x >> separator >> y) && (separator == ',')) {
        auto *curTown = new Point();
        curTown->number = counter++;
        curTown->x = x;
        curTown->y = y;
        towns->push_back(curTown);
    }
    return towns;
}

IntegerSolution *pairMutation(IntegerSolution *parent, std::vector<Point *> *towns) {
    std::vector<int> solution;
    auto *child = new IntegerSolution();
    solution.insert(solution.end(), parent->solution.begin(), parent->solution.end());
    for (int i = 1; i < solution.size(); i++) {
        double path = distance_utils::getPath(solution, towns);
        std::swap(solution[i - 1], solution[i]);
        if (distance_utils::getPath(solution, towns) > path) {
            std::swap(solution[i - 1], solution[i]);
        }
    }

    child->solution = solution;
    return child;
}

IntegerSolution *inversion(IntegerSolution *parent1) {
    std::vector<int> solution1;
    auto *child = new IntegerSolution();

    solution1.insert(solution1.end(), parent1->solution.begin(), parent1->solution.end());
    unsigned long p1 = random() % (solution1.size() / 2);
    unsigned long p2 = random() % (solution1.size() / 2);
    std::reverse(solution1.begin() + p1, solution1.end() - p2);
    child->solution = solution1;
    return child;
}

std::vector<IntegerSolution *> *crossover(IntegerSolution *parent1, IntegerSolution *parent2) {
    unsigned long cutPoint = random() % parent1->solution.size();

    auto *children = new std::vector<IntegerSolution *>();
    auto *child1 = new IntegerSolution();
    auto *child2 = new IntegerSolution();
    std::vector<int> solution1;
    std::vector<int> solution2;
    solution1.insert(solution1.end(), parent1->solution.begin(), parent1->solution.begin() + cutPoint);
    solution2.insert(solution2.end(), parent2->solution.begin(), parent2->solution.begin() + cutPoint);

    for (int gene:parent1->solution) {
        if (std::find(solution2.begin(), solution2.end(), gene) == solution2.end()) {
            solution2.push_back(gene);
        }
    }

    for (int gene:parent2->solution) {
        if (std::find(solution1.begin(), solution1.end(), gene) == solution1.end()) {
            solution1.push_back(gene);
        }
    }

    child1->solution = solution1;
    child2->solution = solution2;
    children->push_back(child1);
    children->push_back(child2);
    return children;
}

double averageFitnessForPopulation(std::vector<IntegerSolution *> *population) {
    double val = 0.0;
    for (auto &solution:*population) {
        val += solution->fitness;
    }
    return val / population->size();
}

