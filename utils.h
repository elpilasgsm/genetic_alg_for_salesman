//
// Created by zaporozhec on 11/27/18.
//
#ifndef TEST_UTILS_H
#define TEST_UTILS_H


#include "Point.h"
#include "IntegerSolution.h"

class utils {
public:
    static int getRandom(int);

    static void printArray(std::vector<Point*> *array);

    static void printArray(std::vector<IntegerSolution*> *array);

    static void deleteArray(std::vector<Point*> *array);

    static void deleteArray(std::vector<IntegerSolution*> *array);
};


#endif //TEST_UTILS_H
