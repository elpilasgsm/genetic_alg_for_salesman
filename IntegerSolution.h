//
// Created by zaporozhec on 11/27/18.
//


#ifndef INTEGER_SOLUTION_T
#define INTEGER_SOLUTION_T

#include <vector>

struct IntegerSolution {
    std::vector<int> solution;
    double fitness;
};

#endif
