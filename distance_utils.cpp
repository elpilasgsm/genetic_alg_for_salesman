//
// Created by zaporozhec on 11/28/18.
//

#include "distance_utils.h"
#include <cmath>

double distance_utils::getDistance(int num1, int num2, std::vector<Point *> *towns) {
    Point *t1 = nullptr, *t2 = nullptr;
    double dist = 0.0;
    if (towns != nullptr) {
        for (auto &t:*towns) {
            if (t->number == num1) {
                t1 = t;
                continue;
            }
            if (t->number == num2) {
                t2 = t;
                continue;
            }
            if (t1 != nullptr && t2 != nullptr) {
                break;
            }
        }
        if (t1 == nullptr || t2 == nullptr) {
            return dist;
        } else {
            dist = sqrt(pow(t1->x - t2->x, 2.0) + pow(t1->y - t2->y, 2.0));
        }

    }
    return dist;
}

double distance_utils::getPath(std::vector<int> solution, std::vector<Point *> *towns) {
    double retVal = 0.0;
    for (int i = 1; i < solution.size(); i++) {
        retVal += getDistance(solution[i - 1], solution[i], towns);
    }
    retVal += getDistance(solution[solution.size() - 1], solution[0], towns);
    return retVal;
}

